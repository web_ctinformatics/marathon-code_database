<!DOCTYPE html>
<html>
<head>
	<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 5px;
  text-align: left;    
}
</style>
</head>
<body>
<table class="table table-inverse">
      <thead>
        <tr>
          <th>id</th>
          <th>logs</th>
        </tr>
      </thead>

      <tbody>
        <% for(var i=0; i < result.length; i++) { %>
       <tr>
         <td><%= result[i]._id %></td>
         <td><%= result[i].logs%></td>
       </tr>
      </tbody>
      <% } %>

    </table>
</body>
</html>

