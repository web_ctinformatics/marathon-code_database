var createError = require('http-errors');

var express = require('express');
const bodyParser = require("body-parser");
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var expressValidator = require('express-validator');
var expressSession = require('express-session');
const cors = require('cors');
var userforgetdesign  = require('./routes/userforget');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var userForgetRouter = require('./routes/userforget');
var adminRouter = require("./routes/admin");
var addRouter = require('./routes/adding');
//var io = require('socket.io')(http);
//var request = require('request');
const {mongoose} = require("./db");
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use('*', cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/uploads", express.static(path.join("./uploads")));
app.use('/userdesign',userforgetdesign );
//  app.use('/', indexRouter);
app.use('/forgetuserpassword',userForgetRouter)
// app.use('/users', usersRouter);
// app.use('/login',loginRouter);
app.use('/',usersRouter);
app.use('/admin',adminRouter);
// app.use("/del",addRouter);
// app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json());
// app.use(expressValidator());
app.use(expressSession({secret: 'max', saveUninitialized: false, resave: false}));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
