const mongoose = require("mongoose");

const deviceHistorySchema = mongoose.Schema({
	deviceDataTimeStamp:{type:String},
	deviceLatitude:{type:String},
	deviceLongitude:{type:String},
	batteryVoltage:{type:String},
	batteryPercent:{type:String},
	devicePairCode:{type:String}
});


module.exports = mongoose.model("deviceHistory",deviceHistorySchema);
