var express = require('express');
var router = express.Router();
var pm2 = require('pm2');
var bodyParser = require("body-parser");
const _ = require('lodash');


const bcrypt = require("bcrypt");
let ObjectId = require("mongoose").Types.ObjectId;
const Device = require('../modals/LoginModal');
const DeviceInfo = require('../modals/deviceInfo');
const User = require('../modals/login');
const Log = require('../modals/logs');
const DeviceHistory = require('../modals/deviceHistroy');
const jwt = require('jsonwebtoken');
const checkauth = require('../middleware/check-auth');
var nodemailer = require('nodemailer');
const mongoose = require('mongoose');
var async = require('async');
var crypto = require("crypto");
var request = require("request");
const multer = require('multer');
const mailer = require("nodemailer");


var storage1 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
  }
})



router.post('/firmWare', function(req, res) {
  var upload = multer({
    storage: storage1
  }).single('image')
  upload(req, res, function(err) {
    console.log(req.body);
    var url = req.protocol + "://" + req.get("host");

    DeviceInfo.findOneAndUpdate({ _id: '5fc0ba634f072c7978040846' }, { "$set": {image: req.file.filename, versionName: req.body.versionName, version: req.body.version} }).exec( function (err, salondevices) {
            if (err) {
              senderr(res);
            } else {
                console.log({filename:url + "/uploads/" + req.file.filename, version: req.body.version});
                res.render('successUpload', { result: {filename:url + "/uploads/" + req.file.filename, version: req.body.version}});
            }
    });
   
  })
})




router.post('/addLogs', (req, res, next) => {
    const log = new Log(req.body);
    console.log(req.body, 'logs');
    log.save().then(result => {
        res.json({status: true, message: 'Logs added successfully.', logsId: result._id});
    }).catch(error => {
        res.json({status: false, message: "Something went to be wrong"});
    });
});


router.get('/printLogs', function(req, res) {
Log.find().sort({'_id': -1}).limit(100).then(documents => {
	for (i = 0; i < documents.length; i++) {
    		documents[i]['logs'] = JSON.stringify(documents[i]['logs']);
    }	
    res.render('logs', { result: documents});
	}).catch(error => {
	    console.log(error);
	    res.json({status: false, message: "Something went to be wrong"});
	});
});


router.get('/restart_tcp_server', function(req, res) {

     const log = new Log( {"logs":[10, "111","response",{"data" : { "restart" : true } } ]});
    log.save().then(result => {
        res.json({status: true, message: 'Logs added successfully.', logsId: result._id});
         pm2.restart('index', (err, proc) => {
        res.json({status: true, message: "Restart successfully"});

  })
    }).catch(error => {
        console.log
        res.json({status: false, message: error});
    });

   
 
});

router.get('/stop_tcp_server', function(req, res) {

     const log = new Log( {"logs":[10, "111","response",{"data" : { "restart" : true } } ]});
    log.save().then(result => {
        res.json({status: true, message: 'Logs added successfully.', logsId: result._id});
         pm2.stop('index', (err, proc) => {
        res.json({status: true, message: "Stop successfully"});

  })
    }).catch(error => {
        console.log
        res.json({status: false, message: error});
    });

   
 
});


router.get('/showVersion', function (req, res) {
        var url = req.protocol + "://" + req.get("host");
        DeviceInfo.findOne({ _id: '5fc0ba634f072c7978040846' }).exec( function (err, device) {
            if (err) {
              senderr(res);
            } else {
                console.log(device, 'devicessssss');
            device.image = url + "/uploads/" + device.image;
              res.json({version : device.version,url  : device.image });
            }
          });

});



router.post('/sendMail', function (req, res) {
console.log(mailer);

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'akashbaidya442@gmail.com',
    pass: 'Akash@123456'
  }
});
    console.log("hello");

    console.log(smtpTransport);

var mail = {
    from: "Osilar <akash@gmail.com>",
    to: "vijay225@mailinator.com",
    subject: "Send Email Using Node.js",
    text: "Node.js New world for me",
    html: "<b>Node.js New world for me</b>"
}

transporter.sendMail(mail, function(error, response){
    if(error){
        console.log(error);
        res.json({ status: true, message: "Something went to be wrong"});
    }else{
        res.json({ status: true, message: "Message send successfully"});
    }

    smtpTransport.close();
});

});


router.post('/forgotPassword', async (req, res) => {
    let user = await User.findOneAndUpdate({countryCode: req.body.countryCode, mobilenumber: req.body.mobilenumber}, {$set: {password: req.body.password}});
    if (_.isEmpty(user)) return res.status(201).json({
            status: "0",
            message: 'User not found'
        });

    let result = await User.findOne({ _id: user._id });

    res.status(201).json({
    status: "1",
    message: 'Password updated successfully.',
    userInfo: {
            "_id": result._id,
            "username": result.username,
            "email": result.email,
            "password":result.password,
            "mobilenumber": result.mobilenumber,
            "countryCode":result.countryCode,
             "deviceId": result.deviceId,
            "deviceName": result.deviceName,
            "deviceLatitude": result.deviceLatitude,
            "deviceLongitude": result.deviceLongitude,
            "devicePairCode": result.devicePairCode,
            "batteryVoltage": result.batteryVoltage,
            "batteryPercent": result.batteryPercent,
            "deviceDataTimeStamp": result.deviceDataTimeStamp,
            "powerMode": result.powerMode,
            "operationMode": result.operationMode,
            "geoFenceLatitude": result.geoFenceLatitude,
            "geoFenceLongitude": result.geoFenceLongitude,
            "geoFenceAddress": result.geoFenceAddress,
            "geoFenceRadius": result.geoFenceRadius
        }
    });
});



router.get('/showDocument/:imageName', function(req, res) {
    var url = req.protocol + "://" + req.get("host");
    
    res.json({ status: true, message: "fetch data successfully",url:url + "/uploads/" + req.file.imageName});
  });





 router.get("/UpdatefirmWare", function(req, res) {
    res.render('uploadFile', { title: 'Express' });
 });


// mongoose.set('useFindAndModify', false);


//user Signup

router.get('/', function(req, res, next) {
    return res.json({
        message: 'welcome to osciler'
    });
});

router.post("/userSignup", (req, res, next) => {


    const user = new User(req.body);
    user.save().then(result => {

    	if(!result.devicePairCode) {
	        	result.devicePairCode = "";
	        }
        
        res.status(201).json({
            status: "1",
            message: 'user created',
            userInfo: {
                    "_id": result._id,
                    "username": result.username,
                    "email": result.email,
                    "password":result.password,
                    "mobilenumber": result.mobilenumber,
                    "countryCode":result.countryCode,
                     "deviceId": result.deviceId,
                    "deviceName": result.deviceName,
                    "deviceLatitude": result.deviceLatitude,
                    "deviceLongitude": result.deviceLongitude,
                    "devicePairCode": result.devicePairCode,
                    "batteryVoltage": result.batteryVoltage,
                    "batteryPercent": result.batteryPercent,
                    "deviceDataTimeStamp": result.deviceDataTimeStamp,
                    "powerMode": result.powerMode,
            		"operationMode": result.operationMode,
                    "geoFenceLatitude": result.geoFenceLatitude,
                    "geoFenceLongitude": result.geoFenceLongitude,
                    "geoFenceAddress": result.geoFenceAddress,
                    "geoFenceRadius": result.geoFenceRadius
                }
        });

    }).catch(err => {
        let errorMesssage = "";

        if(err.errors.mobilenumber && err.errors.email) {
            errorMesssage = "This email and mobile number already exists.";
        } else if(err.errors.mobilenumber) {
            errorMesssage = "This mobile number already exists.";
        } else if(err.errors.email) {
            errorMesssage = "This email already exists.";
        }
        console.log(err.errors);
        res.status(201).json({
            status: "0",
            message: errorMesssage,
            userInfo: {}
        });
    });
});

// });



//check mobile


router.post("/checkMobile", (req, res, next) => {
    User.findOne({
        $and: [
        {
             mobilenumber: req.body.mobilenumber
        },
        {
             countryCode: req.body.countryCode
        },
        ]
       
    }).then(user => {
        if (!user) {
            return res.json({
                status: "1",
                message: 'success'
            })


        } else {
            return res.json({
                status: "0",
                message: 'mobile already exist try using another mobile no..'
            });

        }
    });
});




//check end

//check email


router.post("/checkEmail", (req, res, next) => {
    User.findOne({
        email: req.body.email
    }).then(user => {
        if (!user) {
            return res.json({
                status: "1",
                message: 'success'
            })


        } else {
            return res.json({
                status: "0",
                message: 'email already exist try using another email id..'
            });

        }
    });
});


/* error handlers */
senderr = function (res) {
    return res.json({ status: "false", message: 'Something went to be wrong' });
};


router.post('/addUserDevice', function (req, res) {
    if (!req.body.userId || !req.body.devicePairCode || !req.body.deviceName) {
      senderr(res);
    } else {
        User.findOneAndUpdate({ _id: req.body.userId }, { "$set": req.body }).exec(function (err, salondevices) {
            if (err) {
              senderr(res);
            } else {
              res.json({ status: "true", message: 'Registered successfully' });
            }
          });
    }
  });


  router.get('/updateDeviceLatLong/:deviceId/:lat/:long/:Batteryvoltage/:datatimestamp/:batteryPercent', function (req, res) {
        User.updateMany({ devicePairCode: req.params.deviceId }, { "$set": {"deviceLatitude":req.params.lat, "deviceLongitude": req.params.long, "batteryVoltage": req.params.Batteryvoltage, "deviceDataTimeStamp": new Date().getTime(), "batteryPercent": req.params.batteryPercent} }).exec(function (err, salondevices) {
            if (err) {
                console.log(err);
              senderr(res);
            } else {
                const deviceHistory = new DeviceHistory({"devicePairCode":req.params.deviceId, "deviceLatitude":req.params.lat, "deviceLongitude": req.params.long, "batteryVoltage": req.params.Batteryvoltage, "deviceDataTimeStamp": new Date().getTime(), "batteryPercent": req.params.batteryPercent});
                deviceHistory.save().then(result => {
                    res.json({status: true, message: 'device history added successfully.'});
                }).catch(error => {
                    res.json({status: false, message: "Something went to be wrong"});
                });
            }
          });
  });


   router.get('/addDeviceLatLong/:lat/:long/:Batteryvoltage/:datatimestamp/:deviceId/:username/:password', function (req, res) {
        let deviceHistory = new DeviceHistory(
            {
                "latitude":req.params.lat,
                "longitude": req.params.long,
                "batteryVoltage": req.params.Batteryvoltage,
                "datatimestamp": req.params.datatimestamp,
                "devicePairCode": req.params.deviceId,
                "username": req.params.username,
                "password": req.params.password,
            });

        deviceHistory.save(function (err) {
            if (err) {
              res.json(err);
            }
            else {
                res.json({ status: "true", message: 'device history added successfully.' });
            }
        });
  });


  router.get('/user_list', function (req, res) {
    User.count(function (err, count) {
      if (err) res.json({ status: "false", message: "User not found" });
      else {
        User.find().exec(function (err, users) {
          if (!err) { res.json({ status: "true", result: users }); }
          else {
            res.json({ status: "false", message: "Something went to be wrong" });
          }
        });
      }
    });
  });


  router.post('/devicesHistory', function (req, res) {
        let query;
        if(req.body.date) {
            query =  DeviceHistory.find({$and: [{"deviceDataTimeStamp": {$gte: req.body.date}},{devicePairCode: req.body.deviceId}]}).sort({'_id': -1});
        } else {
            query =  DeviceHistory.find({devicePairCode: req.body.deviceId});
        }

        query.exec(function (err, devices) {
          if (!err) { 
            if(devices.length > 0) {
                res.json({ status: "1",message: 'Device history fetch successfully.', result: devices});
            } else {
                res.json({ status: "1",message: 'Data not found', result: devices });
            }
        }
          else {
            res.json({ status: "0", message: "Something went to be wrong" });
          }
        });
  });





//check end


//userLogin 
router.post("/userLogin", (req, res, next) => {
    User.findOne({
        $and: [{
            email: req.body.email
        }, {
            password: req.body.password
        }]
    }).exec(async function (err, user) {
        
        if (!user) {
            return res.json({
                status: "0",
                message: 'Wrong email or password'
            });


        } else {

            if (!user.status) {
                return res.json({
                    status: "0",
                    message: 'your account has been blocked'
                });
            }





            var deviceId = "";
            var deviceName = "";
       // var battery = "";
        //var batteryPercent="";
            if (user.deviceId != null) {
                deviceId = user.deviceId;
                deviceName = user.deviceName;
        //battery = user.batteryVoltage;
        //batteryPercent = user.batteryPercent;
            } else {
                deviceId = "";
                deviceName = "";
        // battery = "";
        //batteryPercent="";
            }
            await delayedLogUserToken(user.id, req.body.deviceToken);
            console.log(user);

            if(!user.devicePairCode) {
            	user.devicePairCode = "";
            }
            return res.json({
                status: "1",
                message: 'login successfully',
                userInfo: {
                    "_id": user._id,
                    "username": user.username,
                    "email": user.email,
                    "password": user.password,
                    "mobilenumber": user.mobilenumber,
                    "countryCode": user.countryCode,
                    
                    "deviceId": user.devicePairCode,
                    "deviceName": deviceName,
                    "deviceLatitude": user.deviceLatitude,
                    "deviceLongitude": user.deviceLongitude,
                    "devicePairCode": user.devicePairCode,
                    "batteryVoltage": user.batteryVoltage,
                    "batteryPercent": user.batteryPercent,
                    "deviceDataTimeStamp": user.deviceDataTimeStamp,
                    "powerMode": user.powerMode,
            		"operationMode": user.operationMode,
                    "geoFenceLatitude": user.geoFenceLatitude,
                    "geoFenceLongitude": user.geoFenceLongitude,
                    "geoFenceAddress": user.geoFenceAddress,
                    "geoFenceRadius": user.geoFenceRadius
                    
                }
            })
        }

    });
});



function delayedLogUserToken(userId, deviceToken) {
  return new Promise(function(resolve,reject)
  {
    User.findOneAndUpdate({"_id":userId},{$set:{"deviceToken":deviceToken}},(err,docs)=>{
        resolve(true);
    });
  }); 
}









//add device

router.post("/addDevice", (req, res) => {
    User.findOne({
        _id: req.body._id
    }).then(user => {
        if (!user) {
            return res.json({
                status: "0",
                message: 'id doenot exist'
            });
        } else {
            const user = new User(req.body);


            User.updateMany({
                    "_id": req.body._id
                },

                {
                    $set: user
                }, (err, result) => {
                    if (!err) {

                        User.findOne({
                            _id: req.body._id
                        }).then(user => {

                            return res.json({
                                status: "1",
                                message: 'Device Added sucessfully',
                                result: {
                                    "_id": user._id,
                                    "password": user.password,
                                    "username": user.username,
                                    "email": user.email,
                                    "countryCode": user.countryCode,
                                    "mobilenumber": user.mobilenumber,
                                    "deviceId": user.deviceId,
                                    "deviceName": user.deviceName,
                                    "profileImage": ""
                                }
                            })


                        });

                       } else {
                        res.send(err);
                    }
                }
            );
        }
    });
});


//deviceInfo
router.post('/deviceInformation', (req, res) => {
    User.findOne({
        _id: req.body._id
    }).then(user => {
    var batteryPercent="";
    var battery="";
        if (user.deviceId != null) {
        
        if(user.batteryPercent!=null){
        batteryPercent = user.batteryPercent;
        }
        else{
            batteryPercent ="";
        }
        if(user.batteryVoltage!=null){
        battery = user.batteryVoltage;
        }
        else{
            battery ="";
        }


            return res.json({
                status: "1",
                message: 'Device Information',
                result: {
                    //"email": user.email,
                    //"username": user.username,
                    "deviceId": user.deviceId,
                    "deviceName": user.deviceName,
            "batteryVoltage":battery,
            "batteryPercent":batteryPercent
                }
            });
        } else {
            return res.json({
                status: "0",
        message:"no devices added yet please add"
            });
        }
    });

});


// router.post("/updateVersion", (req, res) => {

//      if (!req.body.version || !req.body.reason) {
//       senderr(res);
//     } else {
//         console.log(req.body);
//         DeviceInfo.findOneAndUpdate({ _id: '5e81aa2b471e0a31599a95c9' }, { "$set": req.body }).exec(function (err, salondevices) {
//             if (err) {
//               senderr(res);
//             } else {
//                 // var version = '0.1';
//                 // var tokenArray = ["Saab", "Volvo", "BMW"];
//                 // sendVersionNotification(version,tokenArray);
//                 // res.json({status: false, message: 'notification send successfully'});
//                 res.json({ status: "true", message: 'update successfully' });
//             }
//           });
//     }
// });


router.post('/updateVersion', function (req, res) {
    if (!req.body.version || !req.body.reason) {
      senderr(res);
    } else {
        DeviceInfo.findOneAndUpdate({ _id: '5fc0ba634f072c7978040846' }, { "$set": req.body }).exec(async function (err, salondevices) {
            if (err) {
              senderr(res);
            } else {
              res.json({ status: "true", message: 'update version successfully'});
            }
          });
    }
});


router.post('/updateBoot', function (req, res) {
    if (!req.body.version || !req.body.reason) {
      senderr(res);
    } else {
        DeviceInfo.findOneAndUpdate({ _id: req.body.deviceId }, { "$set": req.body }).exec(async function (err, salondevices) {
            if (err) {
              senderr(res);
            } else {
            const userList = await delayedLogUser(req.body.userId);
              res.json({ status: "true", message: 'update version successfully',userList: userList});
            }
          });
    }
});

function delayedLogUser(userId) {
  return new Promise(function(resolve,reject)
  {
    User.find({},{"_id":1}, function(err, user) {
         resolve(user);
    });
  }); 
}




router.post("/addVersion", (req, res, next) => {


    const user = new DeviceInfo(req.body);
    user.save().then(result => {
        console.log(user);
        res.status(201).json({
            status: "1",
            message: 'user created',
            userInfo: {
                    "version": '0.1',
                    "reason": 'reasonreason',
                }
        });

    }).catch(err => {
        res.status(201).json({
            status: "0",
            message: 'already exist',
            error: err,
        });
    });
});





//update profile
router.post('/updateProfile', async (req, res) => {
    if(req.body.devicePairCode) {
        let userCheck = await User.find({ devicePairCode: req.body.devicePairCode});
        if (userCheck) {
            let deviceCount =  userCheck.length;
            console.log(deviceCount);
            console.log(typeof deviceCount);

            if(deviceCount > 1) {
                console.log('error');
                return res.status(201).json({
                    
                status: "0",
                message: "Maximum accounts for the device ID reached.",
                }); 
            }
           
        } 
    }
    

    User.findOneAndUpdate({ _id: req.body.userId }, { "$set": req.body }).exec(async function (err, salondevices) {
        if (err) {

            // if(err.codeName) {
            //     errorMesssage = "This Device Pair Code already exists.";
            // } else {
            //     errorMesssage = "Something went to be wrong";
            // }
            
           res.status(201).json({
            status: "0",
            message: errorMesssage,
            });
        } else {
        const data = await delayedLog(req.body.userId);

	    	if(!data.devicePairCode) {
	        	data.devicePairCode = "";
	        }
          res.json({ status: "true", message: 'update profile successfully',userInfo: {
                    "_id": data._id,
                    "username": data.username,
                    "email": data.email,
                    "password": data.password,
                    "mobilenumber": data.mobilenumber,
                    "countryCode": data.countryCode,
                    "deviceId": data.devicePairCode,
                    "deviceName": data.deviceName,
                    "deviceLatitude": data.deviceLatitude,
                    "deviceLongitude": data.deviceLongitude,
                    "devicePairCode": data.devicePairCode,
                    "batteryVoltage": data.batteryVoltage,
                    "batteryPercent": data.batteryPercent,
                    "deviceDataTimeStamp": data.deviceDataTimeStamp,
                    "powerMode": data.powerMode,
            		"operationMode": data.operationMode,
                    "geoFenceLatitude": data.geoFenceLatitude,
                    "geoFenceLongitude": data.geoFenceLongitude,
                    "geoFenceAddress": data.geoFenceAddress,
                    "geoFenceRadius": data.geoFenceRadius  
                }, });
        }
      });
  });



//update profile
router.post('/logout', function (req, res) {
    User.findOneAndUpdate({ _id: req.body.userId }, { "$set": {"deviceToken":""} }).exec(function (err, salondevices) {
        if (err) {
          senderr(res);
        } else {
        
          res.json({ status: "true", message: 'logout successfully'});
        }
      });
  });


router.post('/updateDataByDevice', function (req, res) {
    User.findOneAndUpdate({ devicePairCode: req.body.deviceId }, { "$set": req.body }).exec(async function (err, salondevices) {
        if (err) {
          senderr(res);
        } else {
        const data = await delayedLog(salondevices._id);
          if(!data.devicePairCode) {
	        	data.devicePairCode = "";
	        }
          res.json({ status: "true", message: 'update profile successfully',userInfo: {
                    "_id": data._id,
                    "username": data.username,
                    "email": data.email,
                    "password": data.password,
                    "mobilenumber": data.mobilenumber,
                    "countryCode": data.countryCode,
                    "deviceId": data.devicePairCode,
                    "deviceName": data.deviceName,
                    "deviceLatitude": data.deviceLatitude,
                    "deviceLongitude": data.deviceLongitude,
                    "devicePairCode": data.devicePairCode,
                    "batteryVoltage": data.batteryVoltage,
                    "batteryPercent": data.batteryPercent,
                    "deviceDataTimeStamp": data.deviceDataTimeStamp,
                    "version": data.version,
                    "powerMode": data.powerMode,
            		"operationMode": data.operationMode,
                    "geoFenceLatitude": data.geoFenceLatitude,
                    "geoFenceLongitude": data.geoFenceLongitude,
                    "geoFenceAddress": data.geoFenceAddress,
                    "geoFenceRadius": data.geoFenceRadius 
                }, });
        }
      });
  });



function delayedLog(userId) {
  return new Promise(function(resolve,reject)
  {
    User.findOne({ _id: userId }, function(err, user) {
         resolve(user);
    });
  }); 
}



//update device
router.post('/updateDevice', (req, res) => {
    const user = new User(req.body);

    User.update({
            "_id": req.body._id
        },

        {
            $set: user
            /*{"deviceId":req.body.deviceId,"deviceName":req.body.deviceName,}*/
        }, (err, result) => {
            if (!err) {

                User.findOne({
                    _id: req.body._id
                }).then(user => {
                    console.log(user);

            if(user.batteryPercent!=null){
        batteryPercent = user.batteryPercent;
        }
        else{
            batteryPercent ="";
        }
        if(user.batteryVoltage!=null){
        battery = user.batteryVoltage;
        }
        else{
            battery ="";
        }

                    return res.json({
                        status: "1",
                        message: 'device updated successfullss',
                        result: {"deviceId": user.deviceId,
                         "deviceName": user.deviceName,
            "batteryVoltage":battery,
            "batteryPercent":batteryPercent}
                    })
                });
            } else {
                return res.json({
                    status: "0",
                    message: 'wrong id  notupdated device information successfull'
                })
            }
        })
})


//show profile

router.post('/showProfile', (req, res) => {
    User.findOne({
        _id: req.body._id
    }).then(user => {
        if (!user) {
            return res.json({
                status: "0",
                message: 'wrong Id'
            });
        } else {
        	if(!user.devicePairCode) {
	        	user.devicePairCode = "";
	        }
            return res.json({
                status: "1",
                message: 'your profile',
                userInfo: {
                    "_id": user._id,
                    "username": user.username,
                    "email": user.email,
                    "password":user.password,
                    "mobilenumber": user.mobilenumber,
                    "countryCode":user.countryCode,
                     "deviceId": user.deviceId,
                    "deviceName": user.deviceName,
                    "deviceLatitude": user.deviceLatitude,
                    "deviceLongitude": user.deviceLongitude,
                    "devicePairCode": user.devicePairCode,
                    "batteryVoltage": user.batteryVoltage,
                    "batteryPercent": user.batteryPercent,
                    "deviceDataTimeStamp": user.deviceDataTimeStamp,
                    "powerMode": user.powerMode,
            		"operationMode": user.operationMode,
                    "geoFenceLatitude": user.geoFenceLatitude,
                    "geoFenceLongitude": user.geoFenceLongitude,
                    "geoFenceAddress": user.geoFenceAddress,
                    "geoFenceRadius": user.geoFenceRadius 
                }
            });
        }
    })


});


router.post('/bootup', (req, res) => {
    console.log(req.body);
    var version;
    var adminInfo;
    var url = req.protocol + "://" + req.get("host");
    User.findOne({ devicePairCode: req.body.deviceId },async function(err, user) {
            if (!user) {  
                 return res.json({status: false, message: 'device code not exist in database'})
            }
            if(req.body.reason == 'version') {
              version = req.body.version;
              adminInfo =await delayedAdminInfo();
              if(version == adminInfo.version) {
                   return res.json({time: Math.round(new Date().getTime()/1000)});
              } else {
                    sendVersionNotification(user.deviceToken,adminInfo, url);
                   return res.json({time: Math.round(new Date().getTime()/1000)});
              } 
            }
    });
});


function sendVersionNotification(token,versionInfo,url) {
    
    var options = { method: 'POST',
      url: 'https://fcm.googleapis.com/fcm/send',
      headers: 
       { 'postman-token': '0001d042-8655-255b-595a-48489780ad79',
         'cache-control': 'no-cache',
         'content-type': 'application/json',
         authorization: 'key=AIzaSyDEHMEAQz1KPr9VzGhi_mVSZvYFGvi2XF4' },
      body: '{"registration_ids": ["'+token+'"],"data" : {"title":"Update version","body":"Please update you device version","version" : "'+versionInfo.version+'","versionName" : "'+versionInfo.versionName+'","url" : "'+url + "/uploads/" + versionInfo.image+'",}}' };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      console.log(body);
    });
    return;
}


function delayedAdminInfo() {
  return new Promise(function(resolve,reject)
  {
    DeviceInfo.findOne({ _id: '5fc0ba634f072c7978040846' }).exec( function (err, device) {
            if (err) {
              senderr(res);
            } else {
                 resolve(device);
            }
          });
  }); 
}



///forget password

//forget password
router.post('/forgtdata',(req,res)=>{
    console.log("check");
    User.findOneAndUpdate({"_id":req.body.title},{$set:{"password":req.body.password}},(err,docs)=>{
      if(!err){
        console.log(req.body.title+'s');
      }
      else{
        console.log("not updated");
      }
    });
  
  })
   
  //Email send from here for forget password
  router.get('/indexs', function(req, res, next) {
    res.render('index', { title: 'Express' });
   });
  
  router.post('/sendmail',(req,res,next)=>{
    async.waterfall([
      (done)=>{
        crypto.randomBytes(20,(err,Buffer)=>{
          var token = Buffer.toString('hex');
          console.log(token);
        done(err, token);
        })
      },
    
      (token, done)=> {
            User.findOne({ email: req.body.email }, function(err, user) {
             if (!user) {
              
                 return res.json({status: false, message: 'email were not exist in database'})
                }
        
              user.token = token;
    
           
                user.save(function(err) {
                done(err, token, user);
               });
             });
           },
        
           (token, user, done)=>{
    
           
    
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'sumit.ctinfotech@gmail.com',
          pass: 'Cti@1234'
        }
      });
      
      var mailOptions = {
        from: 'sumit.ctinfotech@gmail.com',
        to:  'msumit.1995@gmail.com',
        subject: 'Reset password through this link',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                  'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                 'http://' + req.headers.host + '/for/' + token + '\n\n' +
                 'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          return res.json({ message: 'check your email and add new password'});
         
        }
      });
    
    
           }
    
    
    
    
    
    ])
    });
  
    //forget password main Api
  
    router.get('/for/:token',(req,res,next)=>{
     
      User.findOne({"token":req.params.token},(err,docs)=>{
      
       if(docs.token === " "){
         console.log('token expire');
       
        res.send("token expire");
       }
       else{
        if(!err){
          console.log(docs._id+'hh');
          User.findOneAndUpdate({"token":req.params.token},{$set:{"token":" "}},(err,docs)=>{
            console.log(docs._id+'hh');
            if(!err){
                 res.render('forgetpassword',{ id: docs._id }); 
            
            
           
            }   else{
              
              console.log("dsds");
          }
          
       });
       
      }else{
       
        console.log("error");
         
      }
    }
      });
      
    
    });



 //socket

    router.post("/socketData", (req, res, next) => {


        
        const socketdata = new Device({
            username:req.body.username,
            password: req.body.password,
            datatimestamp: req.body.datatimestamp,
           latitude:req.body.latitude,
        longitude:req.body.longitude,
        batteryVoltage:req.body.batteryVoltage
        })
        socketdata.save().then(result => {
            //console.log(user);
            res.status(201).json({
                status: "1",
                message: 'data created',
                result: result
            });
    
        }).catch(err => {
            res.status(201).json({
                status: "0",
                message: 'already exist',
                error: err,
            });
        });
    });
    
//update lat long
    router.post('/updateLatlong', (req, res) => {
        const socketdata = new Device(req.body);
    
        Device.update({
                "deviceId": req.body.deviceId
            },
    
            {
                $set: socketdata
                /*{"deviceId":req.body.deviceId,"deviceName":req.body.deviceName,}*/
            }, (err, result) => {
                if (!err) {
    
                    Device.findOne({
                        deviceId: req.body.deviceId
                    }).then(user => {
                        //console.log(user);
                        return res.json({
                            status: "1",
                            message: 'updated successfullss',
                            result: socketdata
                        })
                    });
                } else {
                    return res.json({
                        status: "1",
                        message: 'notupdated successfull'
                    })
                }
            })
    })


//get device detail
router.post('/getDeviceloc', (req, res) => {
    Device.findOne({
        _id: req.body._id
    }).then(device => {
        if (!device) {
            return res.json({
                status: "0",
                message: 'wrong Id'
            });
        } else {
            return res.json({
                status: "1",
                message: 'your profile',
                result: device
            });
        }
    })


});

module.exports = router;
