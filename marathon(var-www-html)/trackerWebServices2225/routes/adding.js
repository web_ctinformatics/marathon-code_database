var express = require('express');
 var router = express.Router();
const bodyParser = require("body-parser");

const bcrypt = require("bcrypt");
let ObjectId = require("mongoose").Types.ObjectId;
const User = require('../modals/login');
const jwt = require('jsonwebtoken');
const checkauth = require('../middleware/check-auth'); 


router.get("/:id",checkauth, (req, res, next) => {
	User.deleteOne({ _id: req.params.id }).then(result => {
	  console.log(result);
	  res.status(200).json({ message: "Post deleted!" });
	});
  });
module.exports = router;