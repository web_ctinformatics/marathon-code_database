const mongoose = require("mongoose");
const uniqueValidator = require('mongoose-unique-validator');

const deviceSchema = mongoose.Schema({
// username:{type:String,required:true},
username:{type:String},
password:{type:String},
datatimestamp:{type:String},
latitude:{type:String},
longitude:{type:String},
batteryVoltage:{type:String}

// 1. userid 2.timestamp 3. battary voltage 4. battary percentage 5. device Id
});

deviceSchema.plugin(uniqueValidator);

module.exports = mongoose.model("device",deviceSchema);
