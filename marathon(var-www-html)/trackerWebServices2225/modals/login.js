const mongoose = require("mongoose");
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
username:{type:String,default: ''},
email:{type:String,default: ''},
mobilenumber:{type:String,default: ''},
countryCode:{type:String,default: ''},
password :{type:String,default: ''},
deviceId:{type:String},
deviceName:{type:String},
devicePairCode:{type:String},
deviceLatitude :{type:String},
deviceLongitude:{type:String},
Latitude :{type:String},
Longitude:{type:String},

profileImage:{type:String},
batteryVoltage:{type:String},
batteryPercent:{type:String,default: ''},
deviceDataTimeStamp:{type:String},
deviceToken:{type:String},
version:{type:String},
powerMode:{type:Boolean,default: false},
geoFenceLatitude:{type:String,default: ''},
geoFenceLongitude:{type:String,default: ''},
geoFenceAddress:{type:String,default: ''},
geoFenceRadius:{type:String,default: ''},
operationMode:{type:Boolean,default: false},
status:{type:Boolean,default: true}

 
//token:{type:String}
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("user",userSchema);
