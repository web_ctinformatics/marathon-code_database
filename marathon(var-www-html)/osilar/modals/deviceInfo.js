const mongoose = require("mongoose");

const deviceInfoSchema = mongoose.Schema({
	version:{type:String},
	reason:{type:String},
	versionName:{type:String},
	image:{type:String}
});


module.exports = mongoose.model("deviceInfo",deviceInfoSchema);
