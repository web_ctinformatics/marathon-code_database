var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const multer = require("multer");



/** schemas */
let Admin = require("../modals/admin");
let User = require('../modals/login');
let DeviceInfo = require('../modals/deviceInfo');
let Log = require('../modals/logs');



var storage1 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
  }
})



router.post('/adminSignup', (req, res, next) => {
    bcrypt.hash(req.body.password, 10).then(hash => {
        const admin = new Admin({
            email: req.body.email,
            password: hash,
            name: req.body.name
        });

        admin.save().then(result => {
            res.json({ status: true, message: 'Admin added successfully.',adminId: result._id});
        }).catch(error => {
            console.log(error);
            res.json({status: false, message: "Something went to be wrongggggg"})    
        })
    });
});


router.post('/adminLogin', (req, res, next) => {
    console.log(req.body);
    let fetchUser;
    Admin.findOne({email: req.body.email}).then(user => {
        if(!user) {
            return res.json({ status: false, message: 'Email id not found.'});
        }

        fetchUser = user;
        return bcrypt.compare(req.body.password, user.password);
    }).then(result => {
        if(!result) {
            return res.json({ status: false, message: 'password is inccorect.'});
        }

        const token = jwt.sign(
            {
                email: fetchUser.email,
                userId: fetchUser._id,
            },
            "secret_this_should_be_longer"
        );

        return res.json({ status: true, message: 'login successfully.',userId: fetchUser._id,email: fetchUser.email,token: token,name: fetchUser.name});
    }).catch(error => {
        res.json({status: false, message: "Something went to be wrong"});
    })
});


router.post('/resetPassword/:adminId', async (req, res, next) => {
    if(req.body.password == req.body.confpassword) {
         const salt = await bcrypt.genSalt(10);
         password = await bcrypt.hash(req.body.password, salt);
         Admin.updateOne({_id: req.params.adminId}, {$set: {password: password}}).then(result => {
            res.json({status: true, message: 'Password reset successfully.'});
        }).catch(error => {
            res.json({status: false, message: "Something went to be wrong"});
        });
    } else {
        res.json({status: false, message: "Password not match."});
    }
});



router.get('/userList', (req, res, next) => {
    User.find().sort({'_id': -1}).then(documents => {
        console.log(documents);
        fetchedUsers = documents;
        return User.count();
    }).then(count => {
         res.json({status: true, message: 'User list fetch successfully.',lists: fetchedUsers});
    }).catch(error => {
        console.log(error);
        res.json({status: false, message: "Something went to be wrong"});
    });
});

router.get('/userDetail/:userId', (req, res, next) => {
    User.findOne({_id: req.params.userId}).then(result => {
        res.json({status: true, message: 'user detail fetch successfully.', info: result});
    }).catch(error => {
        res.json({status: false, message: "Something went to be wrong"});
    });
});


router.post('/updateUserStatus/:userId', (req, res, next) => {
    User.updateOne({_id: req.params.userId}, {$set: req.body}).then(result => {
        console.log(req.body.status);
        if(req.body.status) {
            res.json({status: true, message: 'User unblocked successfully'});
        } else {
            res.json({status: true, message: 'User blocked successfully'});
        }
    }).catch(error => {
        res.json({status: false, message: "Something went to be wrong"});
    });
});



router.get('/versionList', (req, res, next) => {
    DeviceInfo.find().sort({'_id': -1}).then(documents => {
        const url = req.protocol + "://" + req.get("host");
        for (i = 0; i < documents.length; i++) {
                if(documents[i]['image']) {
                    documents[i]['image'] = url + "/uploads/"+documents[i]['image'];
                }
        }
         res.json({status: true, message: 'Version Info fetch successfully.',lists: documents});
    }).catch(error => {
        console.log(error);
        res.json({status: false, message: "Something went to be wrong"});
    });
});

router.get('/versionDetail/:versionId', (req, res, next) => {
    DeviceInfo.findOne({_id: req.params.versionId}).then(result => {
        res.json({status: true, message: 'Version detail fetch successfully.', info: result});
    }).catch(error => {
        res.json({status: false, message: "Something went to be wrong"});
    });
});






router.post('/updateVersion/:versionId',multer({storage: storage1}).single("image"), (req, res, next) => {
    DeviceInfo.findOne({_id: req.params.versionId}).then(customer => {
            if(!customer) {
                return  res.json({status: false, message: 'version not found'});
            }
        return customer;
    }).then(versionInfo => {

        if(req.file) {
            req.body.image = req.file.filename;
        } else {
            req.body.image = versionInfo.image;
        }

        DeviceInfo.updateOne({_id: req.params.versionId}, {$set: req.body}).then(result => {
            res.json({status: true, message: 'Version updated successfully.'});
        }).catch(error => {
            res.json({status: false, message: "Something went to be wrong"});
        });
    }).catch(error => {
        res.json({status: false, message: "Something went to be wrong"});
    });
});



router.get('/countList', async (req, res, next) => {
   let userCount = await User.find().count();
   res.json({status: true, message: "Count fetch successfully.",userCount: userCount});
});


router.get('/printLogs', function(req, res) {
Log.find().sort({'_id': -1}).then(documents => {
    res.json({status: true, message: "Count fetch successfully.",lists: documents});
    }).catch(error => {
        console.log(error);
        res.json({status: false, message: "Something went to be wrong"});
    });
});



module.exports = router;

