var webSocketServer = new (require('ws')).Server({port: (process.env.PORT || 1337)}),
    webSockets = {} // userID: webSocket
var request1 = require('request');

// CONNECT /:userID
// wscat -c ws://localhost:5000/1

webSocketServer.on('connection', function (webSocket, req) {
   var userID = parseInt(req.url.substr(1), 10)
  webSockets[userID] = webSocket
   console.log('connected: ' + userID + ' in ' + Object.getOwnPropertyNames(webSockets))

  // Forward Message
  //
  // Receive               Example
  // [toUserID, text]      [2, "Hello, World!"]
  //
  // Send                  Example
  // [fromUserID, text]    [1, "Hello, World!"]
  webSocket.on('message', function(message) {
	     var messageArray = JSON.parse(message)
     var toUserWebSocket = webSockets[messageArray.deviceId]
    if (toUserWebSocket) {
       toUserWebSocket.send(JSON.stringify(messageArray))
    }
	
	request1('http://159.89.82.215:5000/updateDeviceLatLong/'+messageArray.deviceId+'/'+messageArray.latitude+'/'+messageArray.longitude+'/'+messageArray.Batteryvoltage+'/'+messageArray.datatimestamp+'/');
  request1('http://159.89.82.215:5000/addDeviceLatLong/'+messageArray.latitude+'/'+messageArray.longitude+'/'+messageArray.Batteryvoltage+'/'+messageArray.datatimestamp+'/'+messageArray.deviceId+'/'+messageArray.username+'/'+messageArray.password);
  })

  webSocket.on('close', function () {
    delete webSockets[userID]
    console.log('deleted: ' + userID)
  })
})