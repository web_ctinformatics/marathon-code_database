const net = require('net');
const port = 1338;
const host = 'localhost';
var cors = require('cors');


var app = require('express')();
app.use(cors())

const fs = require("fs")
var privateKey = fs.readFileSync(
  "/etc/letsencrypt/live/marathon.osilar.com/privkey.pem",
  "utf8"
);
var cert = fs.readFileSync(
  "/etc/letsencrypt/live/marathon.osilar.com/cert.pem",
  "utf8"
);
var credentials = { key: privateKey, cert: cert };

var http = require('https').Server(credentials,app);
//var io = require('socket.io')(http);
// var io = require('socket.io')(http, {
//    cors: {
//     origin: '*',
//     withCredentials: true
//    }
//  });
//https://marathon.osilar.com:5000,
const io = require("socket.io")(http, {
  cors: {
    origin: "https://marathon.osilar.com",
    methods: ["GET", "POST"],
     credentials: true
  }
});
 
var request = require('request');
const moment = require('moment-timezone');
var clients = [];
let sockets1 = [];
const server = net.createServer();

// app.all('*', function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
//   res.header('Access-Control-Allow-Headers', 'Content-Type');
//   next();
// });

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});
// e
// socket io client //
app.get('/sender', function(req, res) {res.sendfile('sender.html');});
app.get('/fota', function(req, res) {res.sendfile('tcpclient.html');});


app.get('/checkApi', function(req, res) {

    request('http://13.232.90.90:6123/showVersion', function(err, res, body) {
           
                    console.log(body);
                });

});

//connection to socket io
http.listen(process.env.PORT || 6888, function(){console.log('listening on', http.address().port);});


// message from mobile device
io.on('connection', function(socket) {
    socket.on('oprationmode', function(data) {
       console.log(clients);
       if(data.operationMode==3){
          clients[data.deviceId].write('[2,"qwehdhkfiwer","PowerOffOperation",{"operationMode" : 0 , "tbch" :0 }]');

        }else{
                clients[data.deviceId].write('[2,"qwehdhkfiwer","operationMode",{"operationMode":"'+data.operationMode+'","tbch" :'+data.tbch+' }]');


        }

    });


    socket.on('ResetRequest', function(data) {
       
        
          clients[data.deviceId].write('[2,"qwehdhkfiwer","ResetRequest",{"operationMode" : 0 , "tbch" :0 }]');

        

    });
 socket.on('Poweroff', function(data) {
 console.log(data);
 console.log(data.deviceId,'deviceid');
  clients[data.deviceId].write('[2,"qwehdhkfiwer","PowerOffOperation",{"operationMode" : 0 , "tbch" :0 }]');


});

    socket.on('firmwareupdate', function(data1) {
      console.log(data1, 'firmwareupdate');
    sockets1.forEach(function(clienta, index, array){
      //console.log(clienta);
      
clienta.write('[2,"qwehdhkfiwer","FOTA",{"version":"'+data1.version+'","url" : "'+data1.url+'","schedule" : '+data1.schedule+',"Attempt" : '+data1.Attempt+' }]');
  });
});


        socket.on('oprationmodeBroadcast', function(data1) {
          io.emit('oprationmodeBroadcast', data1.tbch);
        sockets1.forEach(function(clienta, index, array){ 
clienta.write('[2,"qwehdhkfiwer","operationMode",{"operationMode":"Active","tbch" :'+data1.tbch+' }]');
  });
});

socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });

 });

 


// connection to tcp
server.listen(port);



  
server.on('connection', function(sock) {
    sockets1.push(sock);
    sock.on('data', function(data) {

    var date = new Date();
    var dateString = moment().tz('Asia/Kolkata').format('YYYY,MM,DD,HH,mm,ss');
    var srdt = data.toString();
    var ordt = JSON.parse(srdt);

    request.post('http://13.232.90.90:6123/addLogs', {
  json: {
   "logs":ordt
    }
}, (error, res, body) => {
  if (error) {
    console.error(error)
    return
  }
  console.log(`statusCode: ${res.statusCode}`)
  console.log(body)
})
     io.sockets.emit('logs','update');

     console.log(ordt);
    if(ordt[2]=='DataPayload2') {
    io.sockets.emit('UserLatLong'+ordt[3].data.deviceId,ordt[3].data);
    sock.write('[3,"'+ordt[1]+'","DataPayload" ,{}]');

    request('http://13.232.90.90:6123/updateDeviceLatLong/'+ordt[3].data.deviceId+'/'+ordt[3].data.latitude+'/'+ordt[3].data.longitude+'/'+ordt[3].data.Batteryvoltage+'/'+ordt[3].data.datatimestamp+'/');
        console.log('http://13.232.90.90:6123/updateDeviceLatLong/'+ordt[3].data.deviceId+'/'+ordt[3].data.latitude+'/'+ordt[3].data.longitude+'/'+ordt[3].data.Batteryvoltage+'/'+ordt[3].data.datatimestamp+'/');


    }

      if(ordt[2]=='DataPayload') {

         latitudeconv = parseFloat(ordt[3].data.latitude);
         degValue_latitudeconv= latitudeconv / 100;
         console.log(degValue_latitudeconv,'degValue_latitudeconv');

        intValue_latitudeconv = parseInt(degValue_latitudeconv);
                 console.log(intValue_latitudeconv,'intValue_latitudeconv');

        decMinutesSeconds = ((degValue_latitudeconv - intValue_latitudeconv)) / 0.60;
                        console.log(decMinutesSeconds,'decMinutesSeconds');


        ordt[3].data.latitude=converted_lat = intValue_latitudeconv+decMinutesSeconds;

console.log(converted_lat,'converted_lat');

         latitudeconv = parseFloat(ordt[3].data.longitude);
         degValue_latitudeconv= latitudeconv / 100;
        intValue_latitudeconv = parseInt(degValue_latitudeconv);
        decMinutesSeconds = ((degValue_latitudeconv - intValue_latitudeconv)) / 0.60;
       ordt[3].data.longitude=converted_long = intValue_latitudeconv+decMinutesSeconds;
console.log(converted_long,'converted_long');

    io.sockets.emit('UserLatLong'+ordt[3].data.deviceId,ordt[3].data);
    sock.write('[3,"'+ordt[1]+'","DataPayload" ,{}]');
         console.log('http://13.232.90.90:6123/updateDeviceLatLong/'+ordt[3].data.deviceId+'/'+ordt[3].data.latitude+'/'+ordt[3].data.longitude+'/'+ordt[3].data.Batteryvoltage+'/'+ordt[3].data.datatimestamp+'/');

    request('http://13.232.90.90:6123/updateDeviceLatLong/'+ordt[3].data.deviceId+'/'+converted_lat+'/'+converted_long+'/'+ordt[3].data.Batteryvoltage+'/'+ordt[3].data.datatimestamp+'/11');
    }

    if(ordt[2]=='Error') {
     sock.write('[3,"'+ordt[1]+'","Error" ,{"status":"accepted","timestamp":"'+dateString+'"}]');

    }

    if(ordt[2]=='BatteryLow') {
     sock.write('[3,"'+ordt[1]+'","BatteryLow" ,{"status":"accepted","timestamp":"'+dateString+'"}]');
     io.sockets.emit('BatteryLow'+ordt[3].deviceId,ordt[3]);
 
    }

    if(ordt[2]=='Heartbeat') {
     sock.write('[3,"'+ordt[1]+'","Heartbeat" ,{"status":"accepted","timestamp":"'+dateString+'"}]');
  
    }

    


    if(ordt[2]=='BootNotification') {
    clients[ordt[3].deviceId]=sock;
     console.log(ordt[3]);
    //io.sockets.emit('checkversion'+ordt[3].deviceId,ordt[3]);
console.log()
    request('http://13.232.90.90:6123/showVersion', function(err, res, body) {
                   var databody = JSON.parse(body);
                                          io.sockets.emit('checkversion'+ordt[3].deviceId,ordt[3]);

                    if(databody.version>ordt[3].version)
                    {
                      console.log(databody.version)
                     console.log(ordt[3].version)


                       // new version availbale notification to mobile
                       io.sockets.emit('updatefirmwareversion'+ordt[3].deviceId,ordt[3]);
                  //sock.write('[3,"'+ordt[1]+'","BootNotification" ,'+body+']');
        // sock.write('[2,"'+ordt[1]+'","FOTA",{"version" : "'+databody.version+'","url" : "'+databody.url+'","schedule" : "now","Attempt" : "5"}');
                    }
                    
                });
    console.log(ordt[3]);
  sock.write('[3,"'+ordt[1]+'","BootNotification" ,{"status":"accepted","timestamp":"'+dateString+'"}]');
    }
    });
 });
